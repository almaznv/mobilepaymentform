(function($, Backbone){
    "use strict";

    console.log("jQuery version: " + $.fn.jquery);
    console.log("Backbone version: " + Backbone.VERSION);

    
    //плагин для маски для jQuery
    $.fn.numericMask = function (maskString, options) {

        var InputNumericMask = function(options) {

            //утилитки
            var utils = {
                clearRegex: function(string) {
                    var symbols = ["/", ".", "*", "+", "?", "|", "(", ")", "[", "]", "{", "}", "\\", "$", "^"];
                    return string.replace(new RegExp("(\\" + symbols.join("|\\") + ")", "g"), "\\$1");
                },
                replaceAt: function(string, index, add) {
                    add = add || "";
                    return string.substr(0, index) + add + string.substr(index + add.length);
                },
                values : function(object) {
                    var result = [];
                    for (var item in object) {
                        result.push(object[item]);
                    }
                    return result;
                },
                contains : function(array, value) {
                    array = array.filter(function(item){
                        return item == value;
                    })
                    return (array.length);
                }
            }

            var //input jQuery элемент
                $el = options.el,
                //символ маски который заменяется вводимимы значениями
                replaceSymbol = "_",
                //спецсимволы маски
                maskSymbols = ["|","-"],
                //маска
                mask = options.mask || "",
                //регулярное выражения для тестирования маски
                maskRegExp = new RegExp("^" + utils.clearRegex(mask).replace(new RegExp(replaceSymbol,"g"), "[0-9]{1}")  + "$"),
                firstStartPos;
            
            this.successCallback = options.success || function(){};
            this.changeCallback = options.change || function(){};

            var keys = {
                backspace : 8,
                tab : 9,
                arrowLeft : 37,
                arrowRight : 38,
                arrowUp : 39,
                arrowDown : 40,
            }

            this.init = function() {
                //определяем позицию откуда можно начать вводить
                //потомучто не факт что в начале нет спецсимволов маски
                this.firstStartPos = this.getStartPos(mask);;
            }

            //берет символ из маски, по указанной позиции
            this.getMaskSymbol = function(pos) {
                return mask[pos];
            }

            //проверка символа, является ли он спецсимволом маски
            this.isMaskSymbol = function(symbol) {
                return utils.contains(maskSymbols, symbol);
            }

            //берем позицию каретки
            this.getCaretPos = function() {
                return $el[0].selectionStart;
            }

            //ставим каретку в позицию begin
            //если end > begin выделяем символы между begin и end
            this.setCaretPos =  function(begin, end) {
                end = end || begin;
                var element = $el[0];
                if (element.setSelectionRange) {
                    element.setSelectionRange(begin, end);
                } else if (element.createTextRange) {
                    var range = element.createTextRange()
                                    .collapse(true)
                                    .moveStart('character', begin)
                                    .moveEnd('character', end)
                                    .select();
                }
            },

            //получаем позицию откуда можно начать вводить
            this.getStartPos = function(value) {
                return value.indexOf(replaceSymbol);
            }

            //назначаем событие для реагирования на успех заполнения
            this.setOnSuccess = function(callback) {
                this.successCallback = callback || function(){};
                return this;
            }

            //назначаем событие для реагирование на ввод значения (исключая успех)
            this.setOnChange = function(callback) {
                this.changeCallback = callback || function(){};
                return this;
            }

            //проверка валидности значения по маске
            this.isValid = function(value) {
                return maskRegExp.test(value)
            }

            //событие при отжатии клавиши
            this.onInputKeyUp = function(e) {
                var value = $el.val();

                //если значени соотвествует маске и каретка на последней позиции запускаем успех
                if ( this.isValid( value ) && e.keyCode != keys.backspace 
                        && this.getCaretPos() == mask.length) {
                    this.successCallback(value);
                    return true;
                } else 
                //если нажат бекспейс имитируем его действие, прыгаем кареткой через спецсимволы маски
                if (e.keyCode == keys.backspace) {
                    var currentPos = this.getCaretPos() - 1;
                    value = utils.replaceAt(value, currentPos , this.getMaskSymbol(currentPos) );
                    $el.val(value);
                    this.setCaretPos(currentPos);
                } 
                this.changeCallback();
            }

            //событие при нажатии клавиши
            this.onInputKeyDown = function(e) {
                //выключаем бекспейс, для имитации  его действия самостоятельно
                if (e.keyCode == keys.backspace) {
                    e.preventDefault();
                    return false;
                }

                //позиция каретки
                var currentPos = this.getCaretPos();
                //значение инпута
                var value = $el.val();

                var key = {
                    code: e.keyCode,
                    isNumeric: new RegExp("[0-9]").test(+String.fromCharCode(e.keyCode)),
                    isSpecialKey: utils.contains( utils.values(keys) , e.keyCode),
                    ctrlKey: e.ctrlKey
                }

                //если ввели число
                if ( key.isNumeric ){
                    if (currentPos >= mask.length) {
                        return false;
                    }

                    //если в поле пусто или позиция каретки меньше позиции откуда можно начать вводить
                    //ставим в инпут исхордное значение маски, ставим текущей позицией - позицию откуда можно начать вводить
                    if (!value || currentPos <= this.firstStartPos ) {
                        value = mask;
                        $el.val(value);
                        currentPos = this.getStartPos(value);
                    }

                    //прыгаем кареткой через спецсимволы маски, чтобы они не заменились при вводе
                    //сдвигаем каретку по мере ввода
                    var curentMaskSymbol = this.getMaskSymbol(currentPos);
                    this.setCaretPos(currentPos, ++currentPos);
                    if ( this.isMaskSymbol(curentMaskSymbol) ) {
                         this.setCaretPos(currentPos, ++currentPos);
                    }
                }

                //игнорируем нажатия клавиш кроме цифр, спец кнопок, и комбинаций с ctrl
                if ( !key.isNumeric && !key.isSpecialKey && !key.ctrlKey) {
                    e.preventDefault();
                    return false;
                }
            }

            //событие при вставке из буфера
            this.onInputPaste = function(e) {
                //получаем текст буфера обмена, window.clipboardData для IE
                var clipboardText = (window.clipboardData) ? 
                                window.clipboardData.getData("Text") : e.originalEvent.clipboardData.getData('Text');

                //оставляем в тексте только цыфры
                clipboardText = clipboardText.replace(new RegExp("(\\D)", "g"), "");

                //берем исходную маску, определяем исходную позицию для вставки
                var value = mask,
                    currentPos = this.getStartPos(value);

                //для каждого символа в тексте
                for (var i = 0; i < clipboardText.length; i++) {
                    var clipbordChar = clipboardText[i];

                    //прыгаем кареткой через спецсимволы маски
                    var curentMaskSymbol = this.getMaskSymbol(currentPos);
                    if ( this.isMaskSymbol(curentMaskSymbol) ) {
                        this.setCaretPos(currentPos, ++currentPos);
                    }

                    //заменяем значение маски на символ из текста буфера
                    value = utils.replaceAt(value, currentPos, clipbordChar);

                    currentPos++;
                }

                //вставляем получившееся значение в инпут
                $el.val(value);
                //выключаем нативной действие вставки
                e.preventDefault();
            }

            this.init();
            
            return this;
        }


        var inputNumericMask = new InputNumericMask({
            el: this,
            mask : maskString
        }).setOnSuccess(options.success).setOnChange(options.change);

        this.on("keydown", inputNumericMask.onInputKeyDown.bind(inputNumericMask));
        this.on("keyup", inputNumericMask.onInputKeyUp.bind(inputNumericMask));
        this.on("paste", inputNumericMask.onInputPaste.bind(inputNumericMask));

        return this;
    }

    //плагин контроля минимального и максимального значений в input[type="number"]
    $.fn.amountMask = function (options) {
        var max = options.max || 5000,
            min = options.min || 1;

        var AmountMask = function(options){
            var $el = options.el,
                max = options.max || 5000,
                min = options.min || 1;

            this.changeCallback = options.change || function() {};

            this.init = function() {
                $el.val(min);
            }

            //назначаем событие для реагирование на ввод значения
            this.setOnChange = function(callback) {
                this.changeCallback = callback || function(){};
                return this;
            }


            //првоерка значения на валидность
            this.isValid = function(value) {
                return (value >= min && value <= max);
            }

            //события после отжатия клавиши
            this.onInputKeyUp = function(e) {
                var value = parseInt($el.val());
                //если не валидно, ставим экстремальные значения
                if ( (value && !this.isValid(value)) || value === 0) {
                    (value < min) ? 
                        value = min :
                            value = max;
                    $el.val(value);
                }

                this.changeCallback(value);
            }

            this.init();
        }

        var amountMask = new AmountMask({
            el: this,
            max: max,
            min: min
        }).setOnChange(options.change);

        this.on("keyup", amountMask.onInputKeyUp.bind(amountMask));

        return this;
    }

    $(document).ready(function(){

        var Model = Backbone.Model.extend({
            url: "/save",
            defaults: {
                phone_number: null,
                amount: 0
            },
            validate: function(attrs, options) {
                if (!attrs.phone_number) {
                    return "Не введен номер телефона";
                }
                if (attrs.amount < 1) {
                  return "Сумма не может быть меньше 1 рубля";
                }
                if (attrs.amount >  5000) {
                  return "Сумма не может быть больше 5000 рублей";
                }
            }
        });

        var View = Backbone.View.extend({
            utils: {
                //форматирование валюты
                //эту функцию писал уже глубоко ночью, поэтому решение в лоб :(
                formatCurrency : function(value){
                    if (!value) return "";
                    var ends = ["ля","ль","лей"], valueOne, valueTwo;
                    value = ""+value;
                    valueOne = value.substr(value.length - 1);
                    valueTwo = value.substr(value.length - 2);

                    if (this.contains([22,33, 44], valueTwo) || (this.contains([2,3,4], valueOne) && !this.contains([12,13,14], valueTwo)) ) {
                        return "руб" + ends[0];
                    } else  if (this.contains([1], valueOne) && !this.contains([11, 12,13,14], valueTwo) ) {
                        return "руб" + ends[1];
                    } else {
                        return "руб" + ends[2];
                    }

                },
                contains : function(array, value) {
                    array = array.filter(function(item){
                        return item == value;
                    })
                    return (array.length);
                }
            },
            bindUi: function() {
                var s = this;
                return {
                    phone_number: s.$el.find(".js_phone_number"),
                    amount: s.$el.find(".js_amount"),
                    submit: s.$el.find(".js_submit"),
                    currency: s.$el.find(".js_currency")
                }
            },
            initialize: function() {
                var s = this;
                this.ui = this.bindUi();

                this.model.on("change:phone_number change:amount", function(){
                    //если данные валидные красим кнопку сабмита
                    if (s.model.isValid()) {
                        s.ui.submit.addClass("fieldset_green_submit_active");
                    } else {
                        s.ui.submit.removeClass("fieldset_green_submit_active");
                    }

                    var amount = s.model.get("amount");
                    s.ui.currency.text(s.utils.formatCurrency(amount));

                });

                //назначаем маску для поля с номером телефона
                this.ui.phone_number.numericMask("+7|___|___-__-__", {
                        success: function (value) {
                            //если поле телефона заполнено, переходим к сумме
                            s.ui.amount.focus();
                            s.model.set("phone_number", value)
                        },
                        change: function (value) {
                            s.model.set("phone_number", s.model.defaults.phone_number);
                        }
                    });

                //назначаем обработчик для поля суммы
                this.ui.amount.amountMask({
                        min: 0,
                        max: 5000,
                        change: function(value) {
                            s.model.set("amount", value || s.model.defaults.amount);
                        }
                    });

                //при клике на самбит формы
                this.ui.submit.on("click", function(e){
                    e.preventDefault();

                    if (!s.model.isValid()) {
                        alert(s.model.validationError);
                    }
                });

                s.ui.currency.text(s.utils.formatCurrency(1));
            }
        });


        new View({
            el: ".js_fieldset",
            model: new Model()
        })

        

    });

}(jQuery, Backbone));